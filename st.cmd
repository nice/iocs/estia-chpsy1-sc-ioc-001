#!/usr/bin/env iocsh.bash
require(modbus)
require(s7plc)
require(calc)
epicsEnvSet(estia-chpsy1-sc-ioc-001_VERSION,"plcfactory")
iocshLoad("$(essioc_DIR)/essioc.iocsh")
iocshLoad("./estia-chpsy1-sc-ioc-001.iocsh","IPADDR=172.30.44.15,RECVTIMEOUT=3000")

#Load alarms database
epicsEnvSet(P,"ESTIA-ChpSy1:")
epicsEnvSet(R1,"Chop-BWC-101:")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R1)")

iocInit()
#EOF
